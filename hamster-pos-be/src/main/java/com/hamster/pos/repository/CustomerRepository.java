package com.hamster.pos.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.hamster.pos.dto.BasicDTO;
import com.hamster.pos.dto.CustomerDTO;
import com.hamster.pos.model.Customer;


public interface CustomerRepository extends CrudRepository<Customer, Long>{
	
	public Customer findByUsernameAndPassword(String username, String password);
	
	public Customer findByEmailAddress(String emailAddress);
	
	@Query("select customer from Customer customer where customer.username = (:userName) AND customer.password = (:password)")
	Optional<Customer> authenticateUser(@Param("userName") String userName, @Param("password") String password);
	
	//public User updatePasswordByEmailAddress(String emailAddress);
}
